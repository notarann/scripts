# Set up (Py)ROOT.                                                                                                                                          
import ROOT
import numpy as np
import math
from ROOT import gStyle

t1 = ROOT.TFile("/eos/experiment/faser/phys/2022/r0013/008933/Faser-Physics-008933-00000-00049-r0013-PHYS.root")
t2 = ROOT.TFile("/eos/user/n/notarann/TB/run/src/Control/CalypsoExample/SimHitExample/share/DAT_ch_73987.root")
t3 = ROOT.TFile("/eos/user/n/notarann/TB/run/src/Control/CalypsoExample/SimHitExample/share/DAT_ch_63987.root")
tree1= t1.Get("nt")

pre0 = ROOT.TH1F("pre0", "pre0", 200, 0, 12)
pre1 = ROOT.TH1F("pre1", "pre1", 200, 0, 12)
TB_pre1 =  ROOT.TH1F("TB_pre1", "TB_pre1", 200, 0, 12)
TB_pre0 =  ROOT.TH1F("TB_pre0", "TB_pre0", 200, 0, 12)
TB_pre1 = t2.Get("Total_Q_in_PS_ch_7")
TB_pre0 = t3.Get("Total_Q_in_PS_ch_6")
print (TB_pre1.GetNbinsX())
print (TB_pre0.GetNbinsX())
nEntries= tree1.GetEntries()
print(nEntries)

for i in range(0, nEntries):
   tree1.GetEntry(i)
   if ( tree1.Track_chi2_per_dof < 25 and tree1.Track_nHits >= 12 and tree1.Track_pz0 > 20000):
      pre0.Fill(tree1.Preshower0_charge)
      pre1.Fill(tree1.Preshower1_charge)

print(pre0.Integral())
print(pre1.Integral())
print(TB_pre0.Integral())
print(TB_pre1.Integral())
pre0.Scale(1/pre0.Integral())
pre1.Scale(1/pre1.Integral())
TB_pre0.Scale(1/TB_pre0.Integral())
TB_pre1.Scale(1/TB_pre1.Integral())

filename = "Muon_8933_vs_3987.pdf"
ROOT.gROOT.SetStyle("ATLAS")
#ROOT.gStyle.SetOptFit(0)

ROOT.gStyle.SetOptStat(0)                                                                                                                                
c = ROOT.TCanvas()
c.Print(filename+'[')

pre1.SetLineColor(2)
TB_pre1.SetLineColor(4)
pre1.SetStats(0)
TB_pre1.SetStats(0)
pre1.Draw("")
TB_pre1.Draw("same")
pre1.GetXaxis().SetTitle("Charge(pC)")
pre1.GetYaxis().SetTitle("Events")
pre1.SetTitle("PS Layer 1")
leg = ROOT.TLegend(0.6,0.6,0.8,0.8)
leg.AddEntry(TB_pre1,"TB MIP Run 3987","L")
leg.AddEntry(pre1,"TI12 MIP Run 8933","L")
leg.Draw()
c.Print(filename)
c.Clear()

pre0.SetLineColor(2)
TB_pre0.SetLineColor(4)
pre0.SetStats(0)
TB_pre0.SetStats(0)
pre0.Draw("")
TB_pre0.Draw("same")
pre0.GetXaxis().SetTitle("Charge(pC)")
pre0.GetYaxis().SetTitle("Events")
pre0.SetTitle("PS Layer 0")
leg = ROOT.TLegend(0.6,0.6,0.8,0.8)
leg.AddEntry(TB_pre0,"TB MIP Run 3987","L")
leg.AddEntry(pre0,"TI12 MIP Run 8933","L")
leg.Draw()
c.Print(filename)
c.Clear()



cend = ROOT.TCanvas()
# Must close file at the end                                                                                                                                
c.Print(filename+']')
