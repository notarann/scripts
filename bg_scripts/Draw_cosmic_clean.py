# Set up (Py)ROOT.                                                                                                                                
import ROOT
import numpy as np
import math
from ROOT import gStyle 

t1 = ROOT.TFile("/afs/cern.ch/work/n/notarann/calypso_BG/run/src/PhysicsAnalysis/NtupleDumper/scripts/comsic.root")
#t1 = ROOT.TFile("/eos/experiment/faser/phys/2022/r0013/009188/Faser-Physics-009188-00000-00049-r0013-PHYS.root")
tree1= t1.Get("nt")
#hist1 = ROOT.TH1F("hist1", "hist1", 200, 0, 3600)
#hist2 = ROOT.TH1F("hist2", "hist2", 200, 0,3600)
#hist3 = ROOT.TH1F("hist3", "hist3", 200, 0,3600)
pre0 = ROOT.TH1F("pre0", "pre0", 100, -5, 20)
pre1 = ROOT.TH1F("pre1", "pre1", 100, -5, 20)
caloTot = ROOT.TH1F("caloTot", "caloTot", 200, -100, 200)
calo0 = ROOT.TH1F("calo0", "calo0", 100, -10, 10)
calo1 = ROOT.TH1F("calo1", "calo1", 100, -10, 10)
calo2 = ROOT.TH1F("calo2", "calo2", 100, -10, 10)
calo3 = ROOT.TH1F("calo3", "calo3", 100, -10, 10)
timing0= ROOT.TH1F("timing0", "timing0", 200, -10, 50)
timing1= ROOT.TH1F("timing1", "timing1", 200, -10, 50)
timing2= ROOT.TH1F("timing2", "timing2", 200, -10, 50)
timing3= ROOT.TH1F("timing3", "timing3", 200, -10, 50)
veto10= ROOT.TH1F("veto10", "veto10", 500, -10, 200)
veto20=ROOT.TH1F("veto20", "veto20", 500, -10, 200)
veto21=ROOT.TH1F("veto21", "veto21", 500, -10, 200)
#tree1.Project("hist1","distanceToInboundB1")
#tree1.Project("hist2","distanceToCollidingBCID")
#tree1.Project("hist3","distanceToUnpairedB1")
#ROOT.gStyle.SetOptStat(111111111)
pre01 = ROOT.TH1F("1pre0", "1pre0", 100, -5, 20)
pre11 = ROOT.TH1F("1pre1", "1pre1", 100, -5, 20)
caloTot1 = ROOT.TH1F("1caloTot", "1caloTot", 200, -100, 200)
caloTot1_weird = ROOT.TH1F("1caloTot_weird", "1caloTot_weird", 200, -100, 200)
eventID_weird = ROOT.TH1F("eventID_weird", "eventID_weird", 10000, 0, 2200000)
caloTot1_onlystats = ROOT.TH1F("onlyStats", "onlyStats", 200, -100, 200)
calo0_raw_charge = ROOT.TH1F("raw_charge", "raw_charge", 200, -100, 200)
calo0_charge = ROOT.TH1F("charge", "charge", 200, -100, 200)
timing01= ROOT.TH1F("1timing0", "1timing0", 200, -10, 50)
timing11= ROOT.TH1F("1timing1", "1timing1", 200, -10, 50)
timing21= ROOT.TH1F("1timing2", "1timing2", 200, -10, 50)
timing31= ROOT.TH1F("1timing3", "1timing3", 200, -10, 50)
veto101= ROOT.TH1F("1veto10", "1veto10", 500, -10, 200)
veto201=ROOT.TH1F("1veto20", "1veto20", 500, -10, 200)
veto211=ROOT.TH1F("1veto21", "1veto21", 500, -10, 200)
pre04 = ROOT.TH1F("4pre0", "4pre0", 100, -5, 20)
pre14 = ROOT.TH1F("4pre1", "4pre1", 100, -5, 20)
caloTot4 = ROOT.TH1F("4caloTot", "4caloTot", 200, -100, 200)
timing04= ROOT.TH1F("4timing0", "4timing0", 200, -10, 50)
timing14= ROOT.TH1F("4timing1", "4timing1", 200, -10, 50)
timing24= ROOT.TH1F("4timing2", "4timing2", 200, -10, 50)
timing34= ROOT.TH1F("4timing3", "4timing3", 200, -10, 50)
veto104= ROOT.TH1F("4veto10", "4veto10", 500, -10, 200)
veto204=ROOT.TH1F("4veto20", "4veto20", 500, -10, 200)
veto214=ROOT.TH1F("4veto21", "4veto21", 500, -10, 200)



def is_good_waveform( waveform_status):
    if int(waveform_status) in [0,4]:
        return True
    return False
m_TotCalo11 = float()
m_TotCalo4=float()
m_TotCalo1=float()
m_TotCalo=float()      
weird= float()
trig1=0
trig4 = 0
statusCut =0 
nEntries= tree1.GetEntries()
print(nEntries)
dInboundB1Cut = 0 
for i in range(0, nEntries):
     tree1.GetEntry(i)
     m_TotCalo1= 0
     m_TotCalo11 = 0
     if ( (tree1.TAP&1) != 0):
        caloTot1.Fill(tree1.Calo_total_E_EM/1000)
        if (is_good_waveform(tree1.Preshower0_status)):
         pre01.Fill(tree1.Preshower0_charge)
        if( is_good_waveform(tree1.Preshower1_status)):
         pre11.Fill(tree1.Preshower1_charge)
        if ( is_good_waveform(tree1.Calo0_status) and tree1.Calo0_raw_peak>4): #filling the calo triggered events with just status check+raw_peak>4
         m_TotCalo1 += tree1.Calo0_E_EM
        if ( is_good_waveform(tree1.Calo1_status) and tree1.Calo1_raw_peak>4):
          m_TotCalo1 +=  tree1.Calo1_E_EM
        if (is_good_waveform(tree1.Calo2_status) and tree1.Calo2_raw_peak>4):
         m_TotCalo1 += tree1.Calo2_E_EM
        if ( is_good_waveform(tree1.Calo3_status) and tree1.Calo3_raw_peak>4):
         m_TotCalo1 += tree1.Calo3_E_EM                                              
#          print("Weird Calo event: Run Number: "+str(tree1.run)+", Event ID: "+str(tree1.eventID)+", Calo0_raw_peak: "+ str(tree1.Calo0_raw_peak)+", Calo0_raw_charge: "+str(tree1.Calo0_raw_charge)+", Calo0_charge: "+str(tree1.Calo0_charge))                                          
        caloTot1_weird.Fill(m_TotCalo1/1000)
        if ( is_good_waveform(tree1.Calo0_status)  ): ##filling the calo triggered events with just status check 
         m_TotCalo11 += tree1.Calo0_E_EM
        if ( is_good_waveform(tree1.Calo1_status)  ):       
          m_TotCalo11 += tree1.Calo1_E_EM
        if (  is_good_waveform(tree1.Calo2_status)  ):
         m_TotCalo11 += tree1.Calo2_E_EM
        if ( is_good_waveform(tree1.Calo3_status) ):
         m_TotCalo11 += tree1.Calo3_E_EM                                                                                                                                       
        caloTot1_onlystats.Fill(m_TotCalo11/1000)
        if (is_good_waveform(tree1.Timing0_status) ):
         timing01.Fill(tree1.Timing0_charge)
        if (is_good_waveform(tree1.Timing1_status) ):
         timing11.Fill(tree1.Timing1_charge)
        if (is_good_waveform(tree1.Timing2_status)):
         timing21.Fill(tree1.Timing2_charge)
        if (is_good_waveform(tree1.Timing3_status) ):
         timing31.Fill(tree1.Timing3_charge)
        if (is_good_waveform(tree1.VetoSt10_status) ):
         veto101.Fill(tree1.VetoSt10_charge)
        if (is_good_waveform(tree1.VetoSt20_status)):
          veto201.Fill(tree1.VetoSt20_charge)
        if  (is_good_waveform(tree1.VetoSt21_status)):
          veto211.Fill(tree1.VetoSt21_charge)
        trig1+=1

filename = "008773_008953_cosmic_check.pdf"
ROOT.gROOT.SetStyle("ATLAS")
c = ROOT.TCanvas()
c.Print(filename+'[')


c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
caloTot1_onlystats.SetLineColor(2)
caloTot1.SetLineColor(4)                                                                                                                                                                                      
caloTot1.SetFillStyle(3001)
caloTot1_onlystats.SetFillStyle(3001)
caloTot1.SetFillColorAlpha(4, 0.35)
caloTot1_onlystats.SetFillColorAlpha(2, 0.35)                                                                                                                                                                                                 
caloTot1.Draw()
caloTot1_onlystats.Draw("same")                                                                                                                                                                                                                    
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)                                        
leg.AddEntry(caloTot1_onlystats,"By hand addition of CaloTot","L")
leg.AddEntry(caloTot1,"CaloTot from ntuples directly","L")

leg.Draw()

caloTot1.GetXaxis().SetTitle("CaloTot_Energy (GeV))")
caloTot1.GetYaxis().SetTitle("Events")
caloTot1_onlystats.SetStats(0)                                                                                                                                                                                                                      
caloTot1.SetStats(0)
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
caloTot1_weird.SetLineColor(2)
caloTot1_onlystats.SetLineColor(4)                                                                                                       
caloTot1_onlystats.SetFillStyle(3001)
caloTot1_weird.SetFillStyle(3001)
caloTot1_onlystats.SetFillColorAlpha(4, 0.35)
caloTot1_weird.SetFillColorAlpha(2, 0.35)
caloTot1_onlystats.Draw()
caloTot1_weird.Draw("same")                                                                                                          
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)
                                                                                         
leg.AddEntry(caloTot1_onlystats,"only status 0/4","L")
leg.AddEntry(caloTot1_weird," status selection+raw_peak>4","L")

leg.Draw()

caloTot1_onlystats.GetXaxis().SetTitle("CaloTot_Energy (GeV))")
caloTot1_onlystats.GetYaxis().SetTitle("Events")
caloTot1_weird.SetStats(0)                                                                                                            
caloTot1_onlystats.SetStats(0)
c.Print(filename)
c.Clear()




cend = ROOT.TCanvas()
# Must close file at the end                                                                                                                      
c.Print(filename+']')
