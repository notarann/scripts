# Set up (Py)ROOT.                                                                                                                                
import ROOT
import numpy as np
import math
from ROOT import gStyle 

t1 = ROOT.TFile("/afs/cern.ch/work/n/notarann/calypso_BG/run/src/PhysicsAnalysis/NtupleDumper/scripts/comsic.root")
tree1= t1.Get("nt")
#hist1 = ROOT.TH1F("hist1", "hist1", 200, 0, 3600)
#hist2 = ROOT.TH1F("hist2", "hist2", 200, 0,3600)
#hist3 = ROOT.TH1F("hist3", "hist3", 200, 0,3600)
pre0 = ROOT.TH1F("pre0", "pre0", 100, -5, 20)
pre1 = ROOT.TH1F("pre1", "pre1", 100, -5, 20)
caloTot = ROOT.TH1F("caloTot", "caloTot", 100, -10, 10)
timing0= ROOT.TH1F("timing0", "timing0", 200, -10, 50)
timing1= ROOT.TH1F("timing1", "timing1", 200, -10, 50)
timing2= ROOT.TH1F("timing2", "timing2", 200, -10, 50)
timing3= ROOT.TH1F("timing3", "timing3", 200, -10, 50)
veto10= ROOT.TH1F("veto10", "veto10", 500, -10, 200)
veto20=ROOT.TH1F("veto20", "veto20", 500, -10, 200)
veto21=ROOT.TH1F("veto21", "veto21", 500, -10, 200)
#tree1.Project("hist1","distanceToInboundB1")
#tree1.Project("hist2","distanceToCollidingBCID")
#tree1.Project("hist3","distanceToUnpairedB1")
#ROOT.gStyle.SetOptStat(111111111)
pre01 = ROOT.TH1F("1pre0", "1pre0", 100, -5, 20)
pre11 = ROOT.TH1F("1pre1", "1pre1", 100, -5, 20)
caloTot1 = ROOT.TH1F("1caloTot", "1caloTot", 100, -10, 10)
timing01= ROOT.TH1F("1timing0", "1timing0", 200, -10, 50)
timing11= ROOT.TH1F("1timing1", "1timing1", 200, -10, 50)
timing21= ROOT.TH1F("1timing2", "1timing2", 200, -10, 50)
timing31= ROOT.TH1F("1timing3", "1timing3", 200, -10, 50)
veto101= ROOT.TH1F("1veto10", "1veto10", 500, -10, 200)
veto201=ROOT.TH1F("1veto20", "1veto20", 500, -10, 200)
veto211=ROOT.TH1F("1veto21", "1veto21", 500, -10, 200)
pre04 = ROOT.TH1F("4pre0", "4pre0", 100, -5, 20)
pre14 = ROOT.TH1F("4pre1", "4pre1", 100, -5, 20)
caloTot4 = ROOT.TH1F("4caloTot", "4caloTot", 100, -10, 10)
timing04= ROOT.TH1F("4timing0", "4timing0", 200, -10, 50)
timing14= ROOT.TH1F("4timing1", "4timing1", 200, -10, 50)
timing24= ROOT.TH1F("4timing2", "4timing2", 200, -10, 50)
timing34= ROOT.TH1F("4timing3", "4timing3", 200, -10, 50)
veto104= ROOT.TH1F("4veto10", "4veto10", 500, -10, 200)
veto204=ROOT.TH1F("4veto20", "4veto20", 500, -10, 200)
veto214=ROOT.TH1F("4veto21", "4veto21", 500, -10, 200)


m_TotCalo4=float()
m_TotCalo1=float()
m_TotCalo=float()      
trig1=0
trig4 = 0
statusCut =0 
nEntries= tree1.GetEntries()
print(nEntries)
dInboundB1Cut = 0 
for i in range(0, nEntries):
     tree1.GetEntry(i)
   #if (tree1.Calo0_status == 0 or tree1.Calo0_status == 1 ) and (tree1.Calo1_status == 0 or tree1.Calo1_status == 1) and (tree1.Calo2_status == 0 or tree1.Calo2_status == 1) and (tree1.Calo3_status == 0 or tree1.Calo3_status == 1):
#     hist1.Fill(tree1.BCID)
     if (tree1.Preshower0_status == 0 or tree1.Preshower0_status == 4 ):
         pre0.Fill(tree1.Preshower0_charge)
     if(tree1.Preshower1_status == 0 or tree1.Preshower1_status == 4 ):
         pre1.Fill(tree1.Preshower1_charge)
     if (tree1.Calo0_status == 0 or tree1.Calo0_status == 4 ):
         m_TotCalo= tree1.Calo0_charge
     if (tree1.Calo1_status == 0 or tree1.Calo1_status == 4 ):
         m_TotCalo += tree1.Calo1_charge
     if (tree1.Calo2_status == 0 or tree1.Calo2_status == 4 ):
         m_TotCalo += tree1.Calo2_charge
     if (tree1.Calo3_status == 0 or tree1.Calo3_status == 4 ):
         m_TotCalo += tree1.Calo3_charge
     caloTot.Fill(m_TotCalo)
     if (tree1.Timing0_status == 0 or tree1.Timing0_status == 4 ):
        timing0.Fill(tree1.Timing0_charge)
     if (tree1.Timing1_status == 0 or tree1.Timing1_status == 4 ):
        timing1.Fill(tree1.Timing1_charge)
     if (tree1.Timing2_status == 0 or tree1.Timing2_status == 4 ):
        timing2.Fill(tree1.Timing2_charge)
     if (tree1.Timing3_status == 0 or tree1.Timing3_status == 4 ):
        timing3.Fill(tree1.Timing3_charge)
     if (tree1.VetoSt10_status == 0 or tree1.VetoSt10_status == 4 ):
        veto10.Fill(tree1.VetoSt10_charge)
     if (tree1.VetoSt20_status == 0 or tree1.VetoSt20_status == 4 ):
         veto20.Fill(tree1.VetoSt20_charge)
     if (tree1.VetoSt21_status == 0 or tree1.VetoSt21_status == 4 ):
         veto21.Fill(tree1.VetoSt21_charge)
     statusCut +=1

     if ( (tree1.TAP&1) != 0  ):
        if (tree1.Preshower0_status == 0 or tree1.Preshower0_status == 4 ):
         pre01.Fill(tree1.Preshower0_charge)
        if(tree1.Preshower1_status == 0 or tree1.Preshower1_status == 4 ):
         pre11.Fill(tree1.Preshower1_charge)
        if (tree1.Calo0_status == 0 or tree1.Calo0_status == 4 ):
         m_TotCalo1= tree1.Calo0_charge
#        print("calo0"+str(m_TotCalo1))
        if (tree1.Calo1_status == 0 or tree1.Calo1_status == 4 ):
         m_TotCalo1 += tree1.Calo1_charge
#        m_TotCalo1 += 2
#        print("calo1"+str(m_TotCalo))
        if (tree1.Calo2_status == 0 or tree1.Calo2_status == 4 ):
         m_TotCalo1 += tree1.Calo2_charge
#        print(m_TotCalo1)
        if (tree1.Calo3_status == 0 or tree1.Calo3_status == 4 ):
         m_TotCalo1 += tree1.Calo3_charge
#        print(m_TotCalo1)
        caloTot1.Fill(m_TotCalo1)
        if (tree1.Timing0_status == 0 or tree1.Timing0_status == 4 ):
         timing01.Fill(tree1.Timing0_charge)
        if (tree1.Timing1_status == 0 or tree1.Timing1_status == 4 ):
         timing11.Fill(tree1.Timing1_charge)
        if (tree1.Timing2_status == 0 or tree1.Timing2_status == 4 ):
         timing21.Fill(tree1.Timing2_charge)
        if (tree1.Timing3_status == 0 or tree1.Timing3_status == 4 ):
         timing31.Fill(tree1.Timing3_charge)
        if (tree1.VetoSt10_status == 0 or tree1.VetoSt10_status == 4 ):
         veto101.Fill(tree1.VetoSt10_charge)
        if (tree1.VetoSt20_status == 0 or tree1.VetoSt20_status == 4 ):
          veto201.Fill(tree1.VetoSt20_charge)
        if (tree1.VetoSt21_status == 0 or tree1.VetoSt21_status == 4 ):
          veto211.Fill(tree1.VetoSt21_charge)
        trig1+=1

     if ( (tree1.TAP&4) != 0  ):
       if (tree1.Preshower0_status == 0 or tree1.Preshower0_status == 4 ):
         pre04.Fill(tree1.Preshower0_charge)
       if(tree1.Preshower1_status == 0 or tree1.Preshower1_status == 4 ):
         pre14.Fill(tree1.Preshower1_charge)
       if (tree1.Calo0_status == 0 or tree1.Calo0_status == 4 ):
         m_TotCalo4= tree1.Calo0_charge
       
       if (tree1.Calo1_status == 0 or tree1.Calo1_status == 4 ):
         m_TotCalo4 += tree1.Calo1_charge
       
       if (tree1.Calo2_status == 0 or tree1.Calo2_status == 4 ):
         m_TotCalo4 += tree1.Calo2_charge
       
       if (tree1.Calo3_status == 0 or tree1.Calo3_status == 4 ):
         m_TotCalo4 += tree1.Calo3_charge
       caloTot4.Fill(m_TotCalo4)
       
       if (tree1.Timing0_status == 0 or tree1.Timing0_status == 4 ):
        timing04.Fill(tree1.Timing0_charge)
       if (tree1.Timing1_status == 0 or tree1.Timing1_status == 4 ):
        timing14.Fill(tree1.Timing1_charge)
       if (tree1.Timing2_status == 0 or tree1.Timing2_status == 4 ):
        timing24.Fill(tree1.Timing2_charge)
       if (tree1.Timing3_status == 0 or tree1.Timing3_status == 4 ):
        timing34.Fill(tree1.Timing3_charge)
       if (tree1.VetoSt10_status == 0 or tree1.VetoSt10_status == 4 ):
        veto104.Fill(tree1.VetoSt10_charge)
       if (tree1.VetoSt20_status == 0 or tree1.VetoSt20_status == 4 ):
         veto204.Fill(tree1.VetoSt20_charge)
       if (tree1.VetoSt21_status == 0 or tree1.VetoSt21_status == 4 ):
         veto214.Fill(tree1.VetoSt21_charge)
       trig4+=1

#     if tree1.distanceToCollidingBCID <1 and tree1.distanceToCollidingBCID >-1:
#          hist2.Fill(tree1.BCID)
     #     pre0.Fill(tree1.Preshower0_charge)
     #     pre1.Fill(tree1.Preshower1_charge)

#     if- 1==1:
     #hist3.Fill(tree1.BCID)
   
mode = 1111   
print ("passing status cut:" +str(statusCut))
print ("passing calo triggers:" +str(trig1))
print ("passing timing triggers:" +str(trig4))
filename = "008773_008953_cosmic.pdf"
ROOT.gROOT.SetStyle("ATLAS")
#ROOT.gStyle.SetOptFit(111);


c = ROOT.TCanvas()
c.Print(filename+'[')
#hist1.SetLineColor(2)

#hist2.SetLineColor(2)

#hist3.SetLineColor(8)
#hist1.GetXaxis().SetRangeUser(0.0, 4.00)
#hist1.Draw("")


#hist1.GetXaxis().SetTitle("BCID")
#hist1.GetYaxis().SetTitle("Events")

#leg = ROOT.TLegend(0.6,0.6,0.8,0.8)

#leg.AddEntry(hist3,"All BCID","L")
#leg.AddEntry(hist1,"All BCID","L")
#leg.Draw()
#c.Print(filename)

#hist3.Draw("same")
#c.Print(filename)
#hist1.GetXaxis().SetRangeUser(3500,3570)
#hist1.GetYaxis().SetRangeUser(0,1800)

#c.Print(filename)
#c.Clear()


#cend = ROOT.TCanvas()
#c.Clear()
#hist2.Draw("")
#c = ROOT.TCanvas()
#hist2.Draw("")

#hist2.GetXaxis().SetTitle("BCID (D collidinf BCID>1&<1)")
#hist2.GetYaxis().SetTitle("Events")                                                                                                                              
#leg = ROOT.TLegend(0.6,0.6,0.8,0.8)                                                                                                             
                                                                                                                            
#leg.SetFillColor(0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
#leg.AddEntry(hist3,"All BCID","L")                                                                                                     
#leg.AddEntry(hist2,"DistanceCollidingBCID","L")
#leg.Draw()   
#c.Print(filename)
#hist3.Draw("same")
#c.Print(filename)
#hist2.GetXaxis().SetRangeUser(1000,2000)
#hist2.GetYaxis().SetRangeUser(0,1800)
#c.Print(filename)
#c.Clear()
#hist3.Draw("same")
#ROOT.gPad.SetLogy()
#leg = ROOT.TLegend(0.6,0.6,0.8,0.8)
#leg.SetBorderSize(0)
#leg.SetFillColor(0)
#leg.SetFillStyle(0)
#leg.SetTextFont(42)
#leg.SetTextSize(0.035)
#leg.AddEntry(hist1,"DistanceInboundB1","L")
#leg.AddEntry(hist2,"DistanceCollidingBCID","L")
#leg.AddEntry(hist3,"DistanceUnpairedB1","L")
#leg.Draw()
#c.Print(filename)
#c.Clear()


c = ROOT.TCanvas()
pre0.Draw()
pre0.GetXaxis().SetTitle("pre0 (charge0)")
pre0.GetYaxis().SetTitle("Events") 
pre0.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()
c = ROOT.TCanvas()
pre1.Draw()
pre1.GetXaxis().SetTitle("pre1 (charge1)")
pre1.GetYaxis().SetTitle("Events")
pre1.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()
c = ROOT.TCanvas()
caloTot.Draw()
caloTot.GetXaxis().SetTitle("CaloTot (chargeTot))")
caloTot.GetYaxis().SetTitle("Events")
caloTot.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()
c = ROOT.TCanvas()
timing0.Draw()
timing0.GetXaxis().SetTitle("timing0 (charge))")
timing0.GetYaxis().SetTitle("Events")
timing0.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()
c = ROOT.TCanvas()
timing1.Draw()
timing1.GetXaxis().SetTitle("timing1 (charge))")
timing1.GetYaxis().SetTitle("Events")
timing1.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()
c = ROOT.TCanvas()
timing2.Draw()
timing2.GetXaxis().SetTitle("timing2 (charge))")
timing2.GetYaxis().SetTitle("Events")
timing2.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()
c = ROOT.TCanvas()
timing3.Draw()
timing3.GetXaxis().SetTitle("timing3 (charge))")
timing3.GetYaxis().SetTitle("Events")
timing3.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
veto10.Draw()
veto10.GetXaxis().SetTitle("veto10 (charge))")
veto10.GetYaxis().SetTitle("Events")
veto10.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
veto20.Draw()
veto20.GetXaxis().SetTitle("veto20 (charge))")
veto20.GetYaxis().SetTitle("Events")
veto20.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
veto21.Draw()
veto21.GetXaxis().SetTitle("veto21 (charge))")
veto21.GetYaxis().SetTitle("Events")
veto21.SetStats(1)
gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy() 
veto214.SetLineColor(8)
veto211.SetLineColor(2)
veto21.SetLineColor(4)
veto21.SetFillStyle(3001)
veto214.SetFillStyle(3001)
veto21.SetFillColorAlpha(4, 0.35)
veto214.SetFillColorAlpha(8, 0.35)
veto21.Draw()
veto214.Draw("same")
veto211.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(veto21,"all events","L")
leg.AddEntry(veto214,"Passed timing trigger 4","L")
leg.AddEntry(veto211,"Passed calo trigger 1","L")

leg.Draw()

veto21.GetXaxis().SetTitle("veto21 (charge))")
veto21.GetYaxis().SetTitle("Events")
veto21.SetStats(0)
#gStyle.SetOptStat(mode)
c.Print(filename)
c.Clear()


c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
veto204.SetLineColor(8)
veto201.SetLineColor(2)
veto20.SetLineColor(4)
veto20.SetFillStyle(3001)
veto204.SetFillStyle(3001)
veto20.SetFillColorAlpha(4, 0.35)
veto204.SetFillColorAlpha(8, 0.35)
veto20.Draw()
veto204.Draw("same")
veto201.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(veto20,"all events","L")
leg.AddEntry(veto204,"Passed timing trigger 4","L")
leg.AddEntry(veto201,"Passed calo trigger 1","L")

leg.Draw()

veto20.GetXaxis().SetTitle("veto20 (charge))")
veto20.GetYaxis().SetTitle("Events")
veto20.SetStats(0)                                                                                                              
#gStyle.SetOptStat(mode)                                                                                                         
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
veto104.SetLineColor(8)
veto101.SetLineColor(2)
veto10.SetLineColor(4)
veto10.SetFillStyle(3001)
veto104.SetFillStyle(3001)
veto10.SetFillColorAlpha(4, 0.35)
veto104.SetFillColorAlpha(8, 0.35)
veto10.Draw()
veto104.Draw("same")
veto101.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(veto10,"all events","L")
leg.AddEntry(veto104,"Passed timing trigger 4","L")
leg.AddEntry(veto101,"Passed calo trigger 1","L")

leg.Draw()

veto10.GetXaxis().SetTitle("veto10 (charge))")
veto10.GetYaxis().SetTitle("Events")
veto10.SetStats(0)                                                                                                              
#gStyle.SetOptStat(mode)                                                                                                         
c.Print(filename)
c.Clear()




c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
timing34.SetLineColor(8)
timing31.SetLineColor(2)
timing3.SetLineColor(4)
timing3.SetFillStyle(3001)
timing34.SetFillStyle(3001)
timing3.SetFillColorAlpha(4, 0.35)
timing34.SetFillColorAlpha(8, 0.35)
timing3.Draw()
timing34.Draw("same")
timing31.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(timing3,"all events","L")
leg.AddEntry(timing34,"Passed timing trigger 4","L")
leg.AddEntry(timing31,"Passed calo trigger 1","L")

leg.Draw()

timing3.GetXaxis().SetTitle("timing3 (charge))")
timing3.GetYaxis().SetTitle("Events")
timing3.SetStats(0)                                                                                                             
                                                                                                                                 
#gStyle.SetOptStat(mode)                                                                                                        
                                                                                                                                 
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
timing24.SetLineColor(8)
timing21.SetLineColor(2)
timing2.SetLineColor(4)
timing2.SetFillStyle(3001)
timing24.SetFillStyle(3001)
timing2.SetFillColorAlpha(4, 0.35)
timing24.SetFillColorAlpha(8, 0.35)
timing2.Draw()
timing24.Draw("same")
timing21.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(timing2,"all events","L")
leg.AddEntry(timing24,"Passed timing trigger 4","L")
leg.AddEntry(timing21,"Passed calo trigger 1","L")

leg.Draw()

timing2.GetXaxis().SetTitle("timing2 (charge))")
timing2.GetYaxis().SetTitle("Events")
timing2.SetStats(0)                                                                                                                                                        

#gStyle.SetOptStat(mode)                                                                                                                                                   

c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
timing14.SetLineColor(8)
timing11.SetLineColor(2)
timing1.SetLineColor(4)
timing1.SetFillStyle(3001)
timing14.SetFillStyle(3001)
timing14.SetFillColorAlpha(8, 0.35)
timing1.SetFillColorAlpha(4, 0.35)
timing1.Draw()
timing14.Draw("same")
timing11.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(timing1,"all events","L")
leg.AddEntry(timing14,"Passed timing trigger 4","L")
leg.AddEntry(timing11,"Passed calo trigger 1","L")

leg.Draw()

timing1.GetXaxis().SetTitle("timing1 (charge))")
timing1.GetYaxis().SetTitle("Events")
timing1.SetStats(0)                                                                                                                                                       
                                                                                                                                                                           

#gStyle.SetOptStat(mode)                                                                                                                                                  
                                                                                                                                                                           

c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
timing04.SetLineColor(8)
timing01.SetLineColor(2)
timing0.SetLineColor(4)
timing0.SetFillStyle(3001)
timing04.SetFillStyle(3001)
timing0.SetFillColorAlpha(4, 0.35)
timing04.SetFillColorAlpha(8, 0.35)
timing0.Draw()
timing04.Draw("same")
timing01.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(timing0,"all events","L")
leg.AddEntry(timing04,"Passed timing trigger 4","L")
leg.AddEntry(timing01,"Passed calo trigger 1","L")

leg.Draw()

timing0.GetXaxis().SetTitle("timing0 (charge))")
timing0.GetYaxis().SetTitle("Events")
timing0.SetStats(0)                                                                                                                                                                                                              


#gStyle.SetOptStat(mode)                                                                                                                                                                                                         


c.Print(filename)
c.Clear()



c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
caloTot4.SetLineColor(8)
caloTot1.SetLineColor(2)
caloTot.SetLineColor(4)
caloTot.SetFillStyle(3001)
caloTot4.SetFillStyle(3001)
caloTot.SetFillColorAlpha(4, 0.35)
caloTot4.SetFillColorAlpha(8, 0.35)
caloTot.Draw()
caloTot4.Draw("same")
caloTot1.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(caloTot,"all events","L")
leg.AddEntry(caloTot4,"Passed timing trigger 4","L")
leg.AddEntry(caloTot1,"Passed calo trigger 1","L")

leg.Draw()

caloTot.GetXaxis().SetTitle("caloTot (charge))")
caloTot.GetYaxis().SetTitle("Events")
caloTot.SetStats(0)
#veto21.SetStats(1)                                                                                                                                                                                                             
#gStyle.SetOptStat(mode)                                                                                                                                                                                                        
c.Print(filename)
c.Clear()


c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
pre14.SetLineColor(8)
pre11.SetLineColor(2)
pre1.SetLineColor(4)
pre1.SetFillStyle(3001)
pre14.SetFillStyle(3001)
pre1.SetFillColorAlpha(4, 0.35)
pre14.SetFillColorAlpha(8, 0.35)
pre1.Draw()
pre14.Draw("same")
pre11.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(pre1,"all events","L")
leg.AddEntry(pre14,"Passed timing trigger 4","L")
leg.AddEntry(pre11,"Passed calo trigger 1","L")

leg.Draw()

pre1.GetXaxis().SetTitle("pre1 (charge))")
pre1.GetYaxis().SetTitle("Events")
pre1.SetStats(0)
#veto21.SetStats(1)                                                                                                                                                                                                             
#gStyle.SetOptStat(mode)                                                                                                                                                                                                        
c.Print(filename)
c.Clear()

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()
pre04.SetLineColor(8)
pre01.SetLineColor(2)
pre0.SetLineColor(4)
pre0.SetFillStyle(3001)
pre04.SetFillStyle(3001)
pre0.SetFillColorAlpha(4, 0.35)
pre04.SetFillColorAlpha(8, 0.35)
pre0.Draw()
pre04.Draw("same")
pre01.Draw("same")
leg = ROOT.TLegend(0.7,0.7,0.9,0.9)

leg.AddEntry(pre0,"all events","L")
leg.AddEntry(pre04,"Passed timing trigger 4","L")
leg.AddEntry(pre01,"Passed calo trigger 1","L")

leg.Draw()

pre0.GetXaxis().SetTitle("pre0 (charge))")
pre0.GetYaxis().SetTitle("Events")
pre0.SetStats(0)                                                                                                                                                                                                             
#gStyle.SetOptStat(mode)                                                                                                                                                                                                        
c.Print(filename)
c.Clear()

cend = ROOT.TCanvas()
# Must close file at the end                                                                                                                      
c.Print(filename+']')
